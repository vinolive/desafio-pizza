var btEnviar = document.getElementById("buttonEnviar")

var formTipo = document.getElementById("idSelectTipo")

var maior = -1;

var pizzas = []

btEnviar.addEventListener("click", function (event) {

    event.preventDefault();

    if (formTipo.value != 0) {
        var formulario = document.querySelector("#formPizza")

        obtemPizza(formulario)

        document.getElementById("tBodyPizza").textContent = ""


        pizzas.sort(comparePorPrecoCm)

        for (let i = 0; i < pizzas.length; i++) {
            var pizzaTr = montaTr(pizzas[i])

            var tableBody = document.getElementById("tBodyPizza")

            if (pizzas[i].diferencaPerc.value === 0) {
                pizzas[i].diferencaPerc = "Melhor CB"
            }

            tableBody.appendChild(pizzaTr)

            formulario.reset()
        }
    } else {
        alert('Escolha um tipo de pizza')
    }

})

var quadrada = false

formTipo.addEventListener("change", function (event) {
    if (formTipo.value == 2) {
        var grupo = document.querySelector("#selectTamanho")
        grupo.innerHTML = `
        <div class="row">
        <div class="col"><input
        class="form-control"
          type="number"
          name="nmLargura"
          id="idLargura"
          placeholder="Largura"
          required
        /></div>
        <div class="col"><input
        class="form-control"
          type="number"
          name="nmComprimento"
          id="idComprimento"
          placeholder="Comprimento"
          required
        /></div>
        </div>
        `
        quadrada = true;
    }
    else if (formTipo.value == 1) {
        var grupo = document.querySelector("#selectTamanho")
        grupo.innerHTML = `<input
        class="form-control"
        type="number"
        name="nmTamanho"
        id="idTamanho"
        placeholder="Tamanho em CM"
        required
      />`
        quadrada = false;
    }
})

function obtemPizza(formulario) {

    if (!quadrada) {
        var preCm = (3.14 * (Math.pow((formulario.idTamanho.value / 2), 2)))

        preCm = formulario.idValor.value / preCm

        preCm = preCm.toFixed(2)

        var copia = false;

        var difPerc = 0;

        for (let i = 0; i < pizzas.length; i++) {
            if (formulario.idTamanho.value == pizzas[i].tamanho) {
                copia = true;
            }
        }

        if (!copia) {
            let pizza = {
                nome: formulario.idNomeComercial.value,
                tamanho: formulario.idTamanho.value,
                valor: formulario.idValor.value,
                precoCm: preCm,
                diferencaPerc: difPerc
            }
            pizzas.push(pizza)

            var primeira = pizzas.length == 1

            var melhor = 0;

            for (let i = 0; i < pizzas.length; i++) {
                if (primeira) {
                    pizzas[0].diferencaPerc = "Melhor CB"
                } else {
                    if (pizzas[i].precoCm < pizzas[melhor].precoCm) {
                        pizzas[melhor].diferencaPerc = (((pizzas[melhor].precoCm * 100) / pizzas[i].precoCm) - 100).toFixed(2)
                        melhor = i;
                        pizzas[melhor].diferencaPerc = "Melhor CB"
                    } else {
                        pizzas[i].diferencaPerc = (((pizzas[i].precoCm * 100) / pizzas[melhor].precoCm) - 100).toFixed(2)
                    }
                    if(pizzas[i].diferencaPerc==0){
                        pizzas[melhor].diferencaPerc="Melhor CB"
                    }
                    for (let j = 0; j < pizzas.length; j++) {
                        if (j != melhor) {
                            pizzas[j].diferencaPerc = (((pizzas[j].precoCm * 100) / pizzas[melhor].precoCm) - 100).toFixed(2)
                        }
                    }
                }
            }



        } else {
            alert("Já existe uma pizza com esse tamanho!")
        }





    } else {
        var preCm = formulario.idValor.value / (formulario.idLargura.value * formulario.idComprimento.value)
        preCm = preCm.toFixed(2)
        var copia = false;
        var difPerc = 0;
        for (let i = 0; i < pizzas.length; i++) {
            if ((formulario.idLargura.value + " x " + formulario.idComprimento.value) == pizzas[i].tamanho) {
                copia = true;
            }
        }

        if (!copia) {
            let pizza = {
                nome: formulario.idNomeComercial.value,
                tamanho: (formulario.idLargura.value + " x " + formulario.idComprimento.value),
                valor: formulario.idValor.value,
                precoCm: preCm,
                diferencaPerc: difPerc
            }
            pizzas.push(pizza)

            var primeira = pizzas.length == 1

            var melhor = 0;

            for (let i = 0; i < pizzas.length; i++) {
                if (primeira) {
                    pizzas[0].diferencaPerc = "Melhor CB"
                } else {
                    if (pizzas[i].precoCm < pizzas[melhor].precoCm) {
                        pizzas[melhor].diferencaPerc = (((pizzas[melhor].precoCm * 100) / pizzas[i].precoCm) - 100).toFixed(2)
                        melhor = i;
                        pizzas[melhor].diferencaPerc = "Melhor CB"
                    } else {
                        pizzas[i].diferencaPerc = (((pizzas[i].precoCm * 100) / pizzas[melhor].precoCm) - 100).toFixed(2)
                    }
                    for (let j = 0; j < pizzas.length; j++) {
                        if (j != melhor) {
                            pizzas[j].diferencaPerc = (((pizzas[j].precoCm * 100) / pizzas[melhor].precoCm) - 100).toFixed(2)
                        }
                    }
                }
            }



        } else {
            alert("Já existe uma pizza com esse tamanho!")
        }

    }


}

function montaTr(pizza) {
    let pizzaTr = document.createElement("tr")
    pizzaTr.classList.add("trPizza")


    pizzaTr.appendChild(montaTd(pizza.nome, "nomeDaPizza"))
    pizzaTr.appendChild(montaTd(pizza.tamanho, "tamanhoDaPizza"))
    pizzaTr.appendChild(montaTd(pizza.valor, "precoDaPizza"))
    pizzaTr.appendChild(montaTd(pizza.precoCm, "precoPorCm"))
    pizzaTr.appendChild(montaTd(pizza.diferencaPerc, "diferencaPercentual"))

    return pizzaTr
}

function montaTd(dado, classe) {
    let pizzaTd = document.createElement("td")
    pizzaTd.classList.add(classe)
    pizzaTd.textContent = dado

    return pizzaTd
}

function comparePorPrecoCm(a, b) {
    if (a.precoCm < b.precoCm) {
        return -1;
    }
    if (a.precoCm > b.precoCm) {
        return 1;
    }
    return 0;
}

function resetAll() {
    var confirm = window.confirm("Deletar toda tabela?")
    if (confirm) {
        document.getElementById("tBodyPizza").textContent = ""
        while (pizzas.length) {
            pizzas.pop();
        }
    }
}